import IronswornActorDataContent from './IronswornActorDataContent';

export default interface IronswornActorData extends ActorData {
  data: IronswornActorDataContent;
}
