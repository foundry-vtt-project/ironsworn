export default class IRONSWORN {
  public static ASCII = 'ᛁᚱᛟᚾᛋᚹᛟᚱᚾ';

  public static attributes = {
    edge: 'IRONSWORN.Edge',
    heart: 'IRONSWORN.Heart',
    iron: 'IRONSWORN.Iron',
    shadow: 'IRONSWORN.Shadow',
    wits: 'IRONSWORN.Wits',
  };
}
